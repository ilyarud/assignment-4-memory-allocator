//
// Created by ilya on 23.12.23.
//

#include "mem.h"
#include "mem_internals.h"

#include <assert.h>

/*
  - Обычное успешное выделение памяти.
  - Освобождение одного блока из нескольких выделенных.
  - Освобождение двух блоков из нескольких выделенных.
  - Память закончилась, новый регион памяти расширяет старый.
  - Память закончилась, старый регион памяти не расширить из-за другого
    выделенного диапазона адресов, новый регион выделяется в другом месте.
*/

static void test_malloc() {
  void* heap = heap_init(REGION_MIN_SIZE);
  debug_heap(stdout, heap);
  assert(heap != NULL);

  void* ptr = _malloc(123);
  debug_heap(stdout, heap);
  assert(ptr != NULL);

  _free(ptr);
  heap_term();

}

static void test_block_free() {
  void* heap = heap_init(REGION_MIN_SIZE);
  debug_heap(stdout, heap);
  assert(heap != NULL);

  void* ptr = _malloc(123);
  debug_heap(stdout, heap);
  assert(ptr != NULL);

  _free(ptr);
  debug_heap(stdout, heap);
  heap_term();

}

static void test_blocks_free() {
  void* heap = heap_init(REGION_MIN_SIZE);
  debug_heap(stdout, heap);
  assert(heap != NULL);

  void* ptr1 = _malloc(123);
  debug_heap(stdout, heap);
  assert(ptr1 != NULL);

  void* ptr2 = _malloc(123);
  debug_heap(stdout, heap);
  assert(ptr2 != NULL);

  _free(ptr1);
  debug_heap(stdout, heap);
  _free(ptr2);
  debug_heap(stdout, heap);

  heap_term();
}

static void test_heap_grow() {
  void* heap = heap_init(REGION_MIN_SIZE);
  debug_heap(stdout, heap);
  assert(heap != NULL);

  void* ptr1 = _malloc(4096);
  debug_heap(stdout, heap);
  assert(ptr1 != NULL);

  void* ptr2 = _malloc(8129);
  debug_heap(stdout, heap);
  assert(ptr2 != NULL);

  debug_heap(stdout, heap);

  _free(ptr1);
  _free(ptr2);
  heap_term();

}

int main() {

//  test_malloc();
//  test_block_free();
//  test_blocks_free();
//  test_heap_grow();

  return 0;
}